---
title: Introduction to LaikaList DNSBL
date: 2021-08-10 01:55:24
---
# What is LaikaList?
It's a DNS Block-List which provides a list of IP addresses of servers which are related to scamming.

# Sites included
## Tech support fraud
* Fake tech support scams (e.g. Microsoft, Adobe, Google, etc.)
* Fake refund scams (e.g. IRS, UK Government, etc.)
* Fake virus removal scams (e.g. "You have a virus" popups, etc.)

## Customer support fraud
* Fake delivery messages
* Fake "you have made these order" messages

## Prize fraud
* Fake prize give-aways.

## Gamer phishing
* Steam/Origin/PlayStation harvesting sites
* Fake giveaway sites
* Fake 

# How do I use it?
* Point your firewall to http://laikaspacedog.gitlab.io/dnsbl

# Submitting a site (Preparing a pull request)
## Required
 1. **Provide evidence** - Make a post explaining the evidence and how the conclusion was reached.
 2. **Inform abuse** - Inform the (A) DNS Registrar, (B) The hosting provider, (C) Law enforcement if applicable - generate a reference and provide it in your report. (Use a shortId generator - e.g. https://shortunique.id/)
 3. **Document the type of scam** - This can be done by outlining what the person was trying to scam, what it pretended to be.
 
## Optional (But very helpful)
 1. **Archive the page to preserve evidence** - Use archive.is or a similar page to preserve evidence of the site.
 2. **Preserve source** - If you can, provide a copy of the source code of any HTML and JavaScript used for any fake pages.

# Removing a site (Also a pull request)
If you have been unduly targetted with a DNSBL and want to be removed, please raise an issue on the git repository for this page along with evidence and we will remove your site from the DNSBL providing it does not meet the categories listed above.
