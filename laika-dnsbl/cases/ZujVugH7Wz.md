# Case Ref: ZujVugH7Wz
## Type
Fake payment/refund processing (Delivery service)

## Description
Fake phishing site used to extract payments, was used via websites where individuals sell second-hand goods, where fake purchaser directs seller to this website when the seller asks for payment information.

## URLs used:
```
https://olx.ro.payfortranspo.pw/cash<XXXXXXXX>
```

## Domain Lookup data (06/08/2021)
```
--------------------------------------------------------
Type 	Domain Name 	    IP Address 	        TTL
--------------------------------------------------------
A 	    payfortranspo.pw 	213.139.211.8       5 min
```
## Registrar
* BEGET LLC (```http://www.beget.com/```)

## Domain WHOIS data (06/08/2021)
```
Domain Name: PAYFORTRANSPO.PW
Registry Domain ID: D244759887-CNIC
Registrar WHOIS Server: whois.beget.com
Registrar URL: https://beget.com
Updated Date: 2021-08-04T11:10:03.0Z
Creation Date: 2021-08-04T11:10:00.0Z
Registry Expiry Date: 2022-08-04T23:59:59.0Z
Registrar: Beget LLC
Registrar IANA ID: 3806
Domain Status: serverTransferProhibited https://icann.org/epp#serverTransferProhibited
Domain Status: clientTransferProhibited https://icann.org/epp#clientTransferProhibited
Domain Status: addPeriod https://icann.org/epp#addPeriod
Registrant Email: https://whois.nic.pw/contact/payfortranspo.pw/registrant
Admin Email: https://whois.nic.pw/contact/payfortranspo.pw/admin
Tech Email: https://whois.nic.pw/contact/payfortranspo.pw/tech
Name Server: NS2.BEGET.PRO
Name Server: NS2.BEGET.COM
Name Server: NS1.BEGET.PRO
Name Server: NS1.BEGET.COM
DNSSEC: unsigned
Billing Email: https://whois.nic.pw/contact/payfortranspo.pw/billing
Registrar Abuse Contact Email: email@beget.com
Registrar Abuse Contact Phone: +7.8124494053
URL of the ICANN Whois Inaccuracy Complaint Form: https://www.icann.org/wicf/
>>> Last update of WHOIS database: 2021-08-06T07:56:55.0Z <<<

For more information on Whois status codes, please visit https://icann.org/epp

>>> IMPORTANT INFORMATION ABOUT THE DEPLOYMENT OF RDAP: please visit
https://www.centralnic.com/support/rdap <<<

The Whois and RDAP services are provided by CentralNic, and contain
information pertaining to Internet domain names registered by our
our customers. By using this service you are agreeing (1) not to use any
information presented here for any purpose other than determining
ownership of domain names, (2) not to store or reproduce this data in
any way, (3) not to use any high-volume, automated, electronic processes
to obtain data from this service. Abuse of this service is monitored and
actions in contravention of these terms will result in being permanently
blacklisted. All data is (c) CentralNic Ltd (https://www.centralnic.com)

Access to the Whois and RDAP services is rate limited. For more
information, visit https://registrar-console.centralnic.com/pub/whois_guidance.
```