# Case Ref: tyVYXVJTfp
## Type
Fake payment/refund processing (Delivery service)

## Description
Fake phishing site used to extract payments, was used via websites where individuals sell second-hand goods, where fake purchaser directs seller to this website when the seller asks for payment information.

## URLs used
```
https://fancourier.ro.queitbuying.site/track%E2%80%A6
```

## Domain Lookup data (06/08/2021)
```
Type	Domain Name	TTL	Address
A	queitbuying.site	299	45.67.58.182

MX
Type	Domain Name	TTL	Preference	Address
MX	queitbuying.site	299	10	mx1.beget.com.
MX	queitbuying.site	299	20	mx2.beget.com.

NS
Type	Domain Name	TTL	Canonical Name
NS	queitbuying.site	299	ns1.beget.com.
NS	queitbuying.site	299	ns2.beget.com.
NS	queitbuying.site	299	ns1.beget.pro.
NS	queitbuying.site	299	ns2.beget.pro.

Type	Domain Name	TTL	Primary NS	Responsible Email
SOA	queitbuying.site	299	ns1.beget.com.	hostmaster.beget.com.

Type	Domain Name	TTL	Record
TXT	queitbuying.site	299	v=spf1 redirect=beget.com
```
## Registrar
* BEGET LLC (```http://www.beget.com/```)

## Domain WHOIS data (06/08/2021)
```
Domain Name: QUEITBUYING.SITE
Registry Domain ID: D245084767-CNIC
Registrar WHOIS Server: whois.beget.com
Registrar URL: https://beget.com
Updated Date: 2021-08-06T10:29:02.0Z
Creation Date: 2021-08-06T10:28:59.0Z
Registry Expiry Date: 2022-08-06T23:59:59.0Z
Registrar: Beget LLC
Registrar IANA ID: 3806
Domain Status: serverTransferProhibited https://icann.org/epp#serverTransferProhibited
Domain Status: clientTransferProhibited https://icann.org/epp#clientTransferProhibited
Domain Status: addPeriod https://icann.org/epp#addPeriod
Registrant Organization: Privacy Protect, LLC (PrivacyProtect.org)
Registrant State/Province: MA
Registrant Country: US
Registrant Email: Please query the RDDS service of the Registrar of Record identified in this output for information on how to contact the Registrant, Admin, or Tech contact of the queried domain name.
Admin Email: Please query the RDDS service of the Registrar of Record identified in this output for information on how to contact the Registrant, Admin, or Tech contact of the queried domain name.
Tech Email: Please query the RDDS service of the Registrar of Record identified in this output for information on how to contact the Registrant, Admin, or Tech contact of the queried domain name.
Name Server: NS2.BEGET.PRO
Name Server: NS2.BEGET.COM
Name Server: NS1.BEGET.PRO
Name Server: NS1.BEGET.COM
DNSSEC: unsigned
Billing Email: Please query the RDDS service of the Registrar of Record identified in this output for information on how to contact the Registrant, Admin, or Tech contact of the queried domain name.
Registrar Abuse Contact Email: email@beget.com
Registrar Abuse Contact Phone: +7.8124494053
URL of the ICANN Whois Inaccuracy Complaint Form: https://www.icann.org/wicf/
>>> Last update of WHOIS database: 2021-08-06T17:43:09.0Z <<<

For more information on Whois status codes, please visit https://icann.org/epp

>>> IMPORTANT INFORMATION ABOUT THE DEPLOYMENT OF RDAP: please visit
https://www.centralnic.com/support/rdap <<<

The Whois and RDAP services are provided by CentralNic, and contain
information pertaining to Internet domain names registered by our
our customers. By using this service you are agreeing (1) not to use any
information presented here for any purpose other than determining
ownership of domain names, (2) not to store or reproduce this data in
any way, (3) not to use any high-volume, automated, electronic processes
to obtain data from this service. Abuse of this service is monitored and
actions in contravention of these terms will result in being permanently
blacklisted. All data is (c) CentralNic Ltd (https://www.centralnic.com)

Access to the Whois and RDAP services is rate limited. For more
information, visit https://registrar-console.centralnic.com/pub/whois_guidance.
```